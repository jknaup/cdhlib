#    This file is part of libcdh.
#
#    libcdh is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    libcdh is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with libcdh.  If not, see <http://www.gnu.org/licenses/>.

SRCDIR=src
INCLUDEDIR=include
LIBDIR=lib
DOCSRC=doc-src
DOCOUTPUT=api-doc
DOXYGEN=doxygen
DOXYFILE=Doxyfile

.PHONY: cdhlib clean
cdhlib: force_look
	$(MAKE) -C $(SRCDIR) lib
	cp $(SRCDIR)/*.mod $(INCLUDEDIR)
	cp $(SRCDIR)/*.a $(LIBDIR)

clean:
	-rm $(INCLUDEDIR)/*
	-rm $(LIBDIR)/*
	-rm -rf $(DOCOUTPUT)/*
	$(MAKE) -C $(SRCDIR) clean

doc:
	$(DOXYGEN) $(DOXYFILE)
	$(MAKE) -C $(DOCOUTPUT)/latex
	
	
force_look :
	true